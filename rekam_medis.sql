-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 25, 2019 at 09:16 AM
-- Server version: 10.1.40-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rekam_medis`
--

-- --------------------------------------------------------

--
-- Table structure for table `dt_rekam`
--

CREATE TABLE `dt_rekam` (
  `id_rekam` int(11) NOT NULL,
  `kd_petugas` int(11) NOT NULL,
  `no_daftar` int(11) NOT NULL,
  `idpasien` int(11) NOT NULL,
  `tgl_periksa` date NOT NULL,
  `penyakit` varchar(100) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dt_rekam`
--

INSERT INTO `dt_rekam` (`id_rekam`, `kd_petugas`, `no_daftar`, `idpasien`, `tgl_periksa`, `penyakit`, `foto`) VALUES
(2, 5, 2, 1, '2019-07-10', 'Mata', '1563627256464.png'),
(5, 5, 3, 5, '2019-07-10', 'Gigi', '1563844796505.png'),
(6, 5, 4, 1, '2019-07-10', 'Katarak', '1563627256464.png'),
(7, 5, 5, 1, '2019-07-11', 'Ginjal', '1564029916656.png');

-- --------------------------------------------------------

--
-- Table structure for table `kepala`
--

CREATE TABLE `kepala` (
  `id_kepala` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nm_kepala` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kepala`
--

INSERT INTO `kepala` (`id_kepala`, `username`, `password`, `nm_kepala`) VALUES
(1, 'superadmin', '17c4520f6cfd1ab53d8745e84681eb49', 'Suhartono');

-- --------------------------------------------------------

--
-- Table structure for table `pasien`
--

CREATE TABLE `pasien` (
  `idpasien` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nm_pasien` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `gol_darah` varchar(100) NOT NULL,
  `nohp` int(12) NOT NULL,
  `kelamin` enum('Pria','Wanita') NOT NULL,
  `tgl_lahir` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pasien`
--

INSERT INTO `pasien` (`idpasien`, `username`, `password`, `nm_pasien`, `alamat`, `gol_darah`, `nohp`, `kelamin`, `tgl_lahir`) VALUES
(1, 'user', 'user', 'user', 'aa', 'A', 1234, 'Pria', '2019-07-08'),
(3, 'aa', 'aa', 'aa', 'aaa', 'B', 1234, 'Wanita', '2019-07-10'),
(4, 'abcd', 'abcd', 'aassss', 'aaaaa', 'AB', 1234567, 'Pria', '2019-07-09'),
(5, 'qq', 'qq', 'qq', 'qq', 'B', 12345, 'Wanita', '2019-07-09');

-- --------------------------------------------------------

--
-- Table structure for table `pendaftaran_rekam`
--

CREATE TABLE `pendaftaran_rekam` (
  `no_daftar` int(11) NOT NULL,
  `tgl_daftar` date NOT NULL,
  `idpasien` int(11) NOT NULL,
  `kd_poli` int(11) NOT NULL,
  `keluhan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pendaftaran_rekam`
--

INSERT INTO `pendaftaran_rekam` (`no_daftar`, `tgl_daftar`, `idpasien`, `kd_poli`, `keluhan`) VALUES
(2, '2019-07-16', 1, 1, 'asadsdsds'),
(3, '2019-07-16', 3, 1, 'sakit gigi'),
(4, '2019-07-17', 1, 2, 'asnajsjas'),
(5, '2019-07-10', 5, 3, 'aaaaa');

-- --------------------------------------------------------

--
-- Table structure for table `penyakit`
--

CREATE TABLE `penyakit` (
  `idpenyakit` int(11) NOT NULL,
  `nm_penyakit` varchar(100) NOT NULL,
  `keluhan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penyakit`
--

INSERT INTO `penyakit` (`idpenyakit`, `nm_penyakit`, `keluhan`) VALUES
(1, 'Sakit Mata', 'aaaaaa'),
(2, 'Sakit Gigi', 'aaaaaaaaa');

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `kd_petugas` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nm_petugas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `petugas`
--

INSERT INTO `petugas` (`kd_petugas`, `username`, `password`, `nm_petugas`) VALUES
(5, 'aa', 'aa', 'aa'),
(6, 'tatang', '12345', 'Tatang Sutarman'),
(7, 'hhh', 'hhh', 'hhhhhhhh');

-- --------------------------------------------------------

--
-- Table structure for table `poli`
--

CREATE TABLE `poli` (
  `kd_poli` int(11) NOT NULL,
  `nm_poli` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `poli`
--

INSERT INTO `poli` (`kd_poli`, `nm_poli`) VALUES
(1, 'Gigi'),
(2, 'Anak'),
(3, 'Mata'),
(5, 'Anak2');

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_as`
-- (See below for the actual view)
--
CREATE TABLE `v_as` (
`id_rekam` int(11)
,`nm_petugas` varchar(100)
,`no_daftar` int(11)
,`kd_poli` int(11)
,`keluhan` varchar(255)
,`nm_pasien` varchar(255)
,`tgl_periksa` date
,`foto` varchar(255)
,`username` varchar(100)
,`idpasien` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_as1`
-- (See below for the actual view)
--
CREATE TABLE `v_as1` (
`kd_poli` int(11)
,`id_rekam` int(11)
,`nm_petugas` varchar(100)
,`no_daftar` int(11)
,`keluhan` varchar(255)
,`nm_pasien` varchar(255)
,`tgl_periksa` date
,`foto` varchar(255)
,`username` varchar(100)
,`idpasien` int(11)
,`nm_poli` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_insa`
-- (See below for the actual view)
--
CREATE TABLE `v_insa` (
`id_rekam` int(11)
,`kd_petugas` int(11)
,`user_pasien` varchar(100)
,`nm_petugas` varchar(100)
,`no_daftar` int(11)
,`idpasien` int(11)
,`uname_pasien` varchar(100)
,`nm_pasien` varchar(255)
,`tgl_periksa` date
,`penyakit` varchar(100)
,`foto` varchar(255)
,`kd_poli` int(11)
,`keluhan` varchar(255)
,`tgl_daftar` date
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_insaa`
-- (See below for the actual view)
--
CREATE TABLE `v_insaa` (
`kd_poli` int(11)
,`id_rekam` int(11)
,`kd_petugas` int(11)
,`user_pasien` varchar(100)
,`nm_petugas` varchar(100)
,`no_daftar` int(11)
,`idpasien` int(11)
,`uname_pasien` varchar(100)
,`nm_pasien` varchar(255)
,`tgl_periksa` date
,`penyakit` varchar(100)
,`foto` varchar(255)
,`keluhan` varchar(255)
,`tgl_daftar` date
,`nm_poli` varchar(100)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_jadi`
-- (See below for the actual view)
--
CREATE TABLE `v_jadi` (
`id_rekam` int(11)
,`nm_petugas` varchar(100)
,`no_daftar` int(11)
,`kd_poli` int(11)
,`keluhan` varchar(255)
,`nm_pasien` varchar(255)
,`tgl_periksa` date
,`foto` varchar(255)
,`username` varchar(100)
);

-- --------------------------------------------------------

--
-- Structure for view `v_as`
--
DROP TABLE IF EXISTS `v_as`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_as`  AS  select `dt_rekam`.`id_rekam` AS `id_rekam`,`petugas`.`nm_petugas` AS `nm_petugas`,`pendaftaran_rekam`.`no_daftar` AS `no_daftar`,`pendaftaran_rekam`.`kd_poli` AS `kd_poli`,`pendaftaran_rekam`.`keluhan` AS `keluhan`,`pasien`.`nm_pasien` AS `nm_pasien`,`dt_rekam`.`tgl_periksa` AS `tgl_periksa`,`dt_rekam`.`foto` AS `foto`,`pasien`.`username` AS `username`,`pasien`.`idpasien` AS `idpasien` from (((`dt_rekam` join `petugas` on((`petugas`.`kd_petugas` = `dt_rekam`.`kd_petugas`))) join `pendaftaran_rekam` on((`dt_rekam`.`no_daftar` = `pendaftaran_rekam`.`no_daftar`))) join `pasien` on((`pasien`.`idpasien` = `dt_rekam`.`idpasien`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_as1`
--
DROP TABLE IF EXISTS `v_as1`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_as1`  AS  select `v_as`.`kd_poli` AS `kd_poli`,`v_as`.`id_rekam` AS `id_rekam`,`v_as`.`nm_petugas` AS `nm_petugas`,`v_as`.`no_daftar` AS `no_daftar`,`v_as`.`keluhan` AS `keluhan`,`v_as`.`nm_pasien` AS `nm_pasien`,`v_as`.`tgl_periksa` AS `tgl_periksa`,`v_as`.`foto` AS `foto`,`v_as`.`username` AS `username`,`v_as`.`idpasien` AS `idpasien`,`poli`.`nm_poli` AS `nm_poli` from (`v_as` join `poli` on((`v_as`.`kd_poli` = `poli`.`kd_poli`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_insa`
--
DROP TABLE IF EXISTS `v_insa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_insa`  AS  select `dt_rekam`.`id_rekam` AS `id_rekam`,`petugas`.`kd_petugas` AS `kd_petugas`,`petugas`.`username` AS `user_pasien`,`petugas`.`nm_petugas` AS `nm_petugas`,`pendaftaran_rekam`.`no_daftar` AS `no_daftar`,`pasien`.`idpasien` AS `idpasien`,`pasien`.`username` AS `uname_pasien`,`pasien`.`nm_pasien` AS `nm_pasien`,`dt_rekam`.`tgl_periksa` AS `tgl_periksa`,`dt_rekam`.`penyakit` AS `penyakit`,`dt_rekam`.`foto` AS `foto`,`pendaftaran_rekam`.`kd_poli` AS `kd_poli`,`pendaftaran_rekam`.`keluhan` AS `keluhan`,`pendaftaran_rekam`.`tgl_daftar` AS `tgl_daftar` from (((`dt_rekam` join `petugas` on((`petugas`.`kd_petugas` = `dt_rekam`.`kd_petugas`))) join `pendaftaran_rekam` on((`pendaftaran_rekam`.`no_daftar` = `dt_rekam`.`no_daftar`))) join `pasien` on((`pasien`.`idpasien` = `dt_rekam`.`idpasien`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_insaa`
--
DROP TABLE IF EXISTS `v_insaa`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_insaa`  AS  select `v_insa`.`kd_poli` AS `kd_poli`,`v_insa`.`id_rekam` AS `id_rekam`,`v_insa`.`kd_petugas` AS `kd_petugas`,`v_insa`.`user_pasien` AS `user_pasien`,`v_insa`.`nm_petugas` AS `nm_petugas`,`v_insa`.`no_daftar` AS `no_daftar`,`v_insa`.`idpasien` AS `idpasien`,`v_insa`.`uname_pasien` AS `uname_pasien`,`v_insa`.`nm_pasien` AS `nm_pasien`,`v_insa`.`tgl_periksa` AS `tgl_periksa`,`v_insa`.`penyakit` AS `penyakit`,`v_insa`.`foto` AS `foto`,`v_insa`.`keluhan` AS `keluhan`,`v_insa`.`tgl_daftar` AS `tgl_daftar`,`poli`.`nm_poli` AS `nm_poli` from (`v_insa` join `poli` on((`v_insa`.`kd_poli` = `poli`.`kd_poli`))) ;

-- --------------------------------------------------------

--
-- Structure for view `v_jadi`
--
DROP TABLE IF EXISTS `v_jadi`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_jadi`  AS  select `dt_rekam`.`id_rekam` AS `id_rekam`,`petugas`.`nm_petugas` AS `nm_petugas`,`pendaftaran_rekam`.`no_daftar` AS `no_daftar`,`pendaftaran_rekam`.`kd_poli` AS `kd_poli`,`pendaftaran_rekam`.`keluhan` AS `keluhan`,`pasien`.`nm_pasien` AS `nm_pasien`,`dt_rekam`.`tgl_periksa` AS `tgl_periksa`,`dt_rekam`.`foto` AS `foto`,`pasien`.`username` AS `username` from (((`dt_rekam` join `petugas` on((`petugas`.`kd_petugas` = `dt_rekam`.`kd_petugas`))) join `pendaftaran_rekam` on((`dt_rekam`.`no_daftar` = `pendaftaran_rekam`.`no_daftar`))) join `pasien` on((`pasien`.`idpasien` = `dt_rekam`.`idpasien`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dt_rekam`
--
ALTER TABLE `dt_rekam`
  ADD PRIMARY KEY (`id_rekam`);

--
-- Indexes for table `kepala`
--
ALTER TABLE `kepala`
  ADD PRIMARY KEY (`id_kepala`);

--
-- Indexes for table `pasien`
--
ALTER TABLE `pasien`
  ADD PRIMARY KEY (`idpasien`);

--
-- Indexes for table `pendaftaran_rekam`
--
ALTER TABLE `pendaftaran_rekam`
  ADD PRIMARY KEY (`no_daftar`);

--
-- Indexes for table `penyakit`
--
ALTER TABLE `penyakit`
  ADD PRIMARY KEY (`idpenyakit`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`kd_petugas`);

--
-- Indexes for table `poli`
--
ALTER TABLE `poli`
  ADD PRIMARY KEY (`kd_poli`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dt_rekam`
--
ALTER TABLE `dt_rekam`
  MODIFY `id_rekam` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `kepala`
--
ALTER TABLE `kepala`
  MODIFY `id_kepala` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `pasien`
--
ALTER TABLE `pasien`
  MODIFY `idpasien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `pendaftaran_rekam`
--
ALTER TABLE `pendaftaran_rekam`
  MODIFY `no_daftar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `penyakit`
--
ALTER TABLE `penyakit`
  MODIFY `idpenyakit` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `petugas`
--
ALTER TABLE `petugas`
  MODIFY `kd_petugas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `poli`
--
ALTER TABLE `poli`
  MODIFY `kd_poli` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
