<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        $this->load->model('model_login');
    }
    
    public function index()
    {
        $this->load->view('loginPasien');
    }

    public function login2()
    {
        $this->load->view('loginPetugas');
    }

    public function login3()
    {
        $this->load->view('loginKepala');
    }

    public function actLogin()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => $pass
        );
        $cek = $this->model_login->cekLogin("pasien",$where)->num_rows();
        if($cek > 0 ) {
            $dt_session = array(
                'username' => $user,
                'status' => "login"
            );
            $this->session->set_userdata($dt_session);
            redirect("pasien");
        } else {
            echo "Username atau password salah";
        }
    }

    public function actLogin1()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => $pass
        );
        $cek = $this->model_login->cekLogin("petugas",$where)->num_rows();
        if($cek > 0 ) {
            $dt_session = array(
                'username' => $user,
                'status' => "login"
            );
            $this->session->set_userdata($dt_session);
            redirect("admin2");
        } else {
            echo "Username atau password salah";
        }
    }

    public function actLogin2()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $where = array(
            'username' => $user,
            'password' => md5($pass)
        );
        $cek = $this->model_login->cekLogin("kepala",$where)->num_rows();
        if($cek > 0 ) {
            $dt_session = array(
                'username' => $user,
                'status' => "login"
            );
            $this->session->set_userdata($dt_session);
            redirect("admin");
        } else {
            echo "Username atau password salah";
        }
    }

    public function logout2()
    {
        $this->session->sess_destroy();
        redirect('login/login3');
    }

    public function logout1()
    {
        $this->session->sess_destroy();
        redirect('login/login2');
    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login');
    }

}

/* End of file Login.php */

?>