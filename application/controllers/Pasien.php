<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect('login');
        }
        $this->load->model('model_pasien');
        
    }
    
    public function index()
    {
        $data['user'] = $this->model_pasien->getData();
        $data['user1'] = $this->model_pasien->getuser();
        $this->load->view('pasien',$data);
    }

}

/* End of file Pasien.php */

?>