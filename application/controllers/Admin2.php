<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin2 extends CI_Controller {

    
    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect('login/login1');
        }
        $this->load->model('model_petugas');
    }
    
    public function index()
    {
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('pages/petugas/content');
        $this->load->view('pages/petugas/footer');
    }

    public function gotoPoli()
    {
        $data['poli'] = $this->model_petugas->getData1()->result();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/poli',$data);
        $this->load->view('pages/petugas/footer');
    }

    function gotoPasienDetail() 
    {
        $data['dt_pasien'] = $this->model_petugas->getDetail();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('petugas/detail',$data);
        $this->load->view('pages/kepala/footer');
    }

    public function gotoPoliAdd()
    {
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/poli-new');
        $this->load->view('pages/petugas/footer');
    }

    public function gotoPoliEdit($kd_poli)
    {
        $where = array('kd_poli' => $kd_poli);
		$data['polii'] = $this->model_petugas->edit_data($where,'poli')->result();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/poli-edit',$data);
        $this->load->view('pages/petugas/footer');
    }

    public function gotoPasien()
    {
        $data['pasien'] = $this->model_petugas->getData()->result();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/pasien',$data);
        $this->load->view('pages/petugas/footer');
    }

    public function gotoPasienAdd()
    {
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/pasien-new');
        $this->load->view('pages/petugas/footer');
    }

    public function gotoPasienEdit($idpasien)
    {
        $where = array('idpasien' => $idpasien);
		$data['pasien'] = $this->model_petugas->edit_data($where,'pasien')->result();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/pasien-edit',$data);
        $this->load->view('pages/petugas/footer');
    }

    public function gotoHapusPasien($idpasien)
    {
        $where = array('idpasien' => $idpasien);
		$this->model_petugas->deleteData($where,'pasien');
        redirect('admin2/gotoPasien');
    }

    public function gotoHapusPoli($kd_poli)
    {
        $where = array('kd_poli' => $kd_poli);
		$this->model_petugas->deleteData($where,'poli');
        redirect('admin2/gotoPoli');
    }

    public function gotoDaftar()
    {
        $data1['poli'] = $this->model_petugas->getData1()->result();
        $data1['pasien'] = $this->model_petugas->getData()->result();
        $data1['dftr'] = $this->model_petugas->getDaftar();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/daftar',$data1);
        $this->load->view('pages/petugas/footer');
    }

    public function gotoRekam()
    {
        $data1['rekam'] = $this->model_petugas->getRekam();
        $data1['pasien'] = $this->model_petugas->getData()->result();
        $data1['no'] = $this->model_petugas->getData2()->result();
        $data1['no1'] = $this->model_petugas->getNo();
        $data1['penyakit'] = $this->model_petugas->getPenyakit();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/rekam',$data1);
        $this->load->view('pages/petugas/footer');
    }

    public function gotoPenyakit()
    {
        $data1['penyakit'] = $this->model_petugas->getPenyakit();
        $this->load->view('pages/petugas/header');
        $this->load->view('pages/petugas/sidebar');
        $this->load->view('petugas/penyakit',$data1);
        $this->load->view('pages/petugas/footer');
    }

    public function actInputPasien()
    {
        $username = $this->input->post('username');
        $sql = $this->db->query("SELECT username FROM pasien WHERE username='$username'");
        $cek_sql = $sql->num_rows();
        if ($cek_sql > 0) {
            $this->session->set_flashdata('message', 'Username sudah digunakan sebelumnya');
            redirect(site_url('admin2/gotoPasienAdd'));
        } else {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $nama = $this->input->post('nm_pasien');
        $almt = $this->input->post('alamat');
        $darah = $this->input->post('gol_darah');
        $hp = $this->input->post('nohp');
        $jk = $this->input->post('kelamin');
        $tgl = $this->input->post('tgl_lahir');
        
        $data = array(
            'username' => $user,
            'password' => $pass,
            'nm_pasien' => $nama,
            'alamat' => $almt,
            'gol_darah' => $darah,
            'nohp' => $hp,
            'kelamin' => $jk,
            'tgl_lahir' => $tgl
        );

        $this->model_petugas->inputData($data,'pasien');
        redirect('admin2/gotoPasien');
        }
    }

    public function actInputDaftar()
    {
        $tgl = $this->input->post('tgl_daftar');
        $pasien = $this->input->post('idpasien');
        $poli = $this->input->post('kd_poli');
        $keluhan = $this->input->post('keluhan');
        
        $data = array(
            'tgl_daftar' => $tgl,
            'idpasien' => $pasien,
            'kd_poli' => $poli,
            'keluhan' => $keluhan
        );

        $this->model_petugas->inputData($data,'pendaftaran_rekam');
        redirect('admin2/gotoDaftar');
    }

    public function actEditPasien()
    {
        $idpasien = $this->input->post('idpasien');
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $nama = $this->input->post('nm_pasien');
        $almt = $this->input->post('alamat');
        $darah = $this->input->post('gol_darah');
        $hp = $this->input->post('nohp');
        $jk = $this->input->post('kelamin');
        $tgl = $this->input->post('tgl_lahir');
        
        $data = array(
            'username' => $user,
            'password' => $pass,
            'nm_pasien' => $nama,
            'alamat' => $almt,
            'gol_darah' => $darah,
            'nohp' => $hp,
            'kelamin' => $jk,
            'tgl_lahir' => $tgl
        );

        $where = array(
            'idpasien' => $idpasien
        );

        $this->model_petugas->update_data($where,$data,'pasien');
        redirect('admin2/gotoPasien');
    }

    public function actInputPoli()
    {
        $nama = $this->input->post('nm_poli');
        $kode = $this->input->post('kd_poli');

        $data = array(
            'nm_poli' => $nama
        );

        $this->model_petugas->inputData($data,'poli');
        redirect('admin2/gotoPoli');
    }

    public function actEditPoli()
    {
        $nama = $this->input->post('nm_poli');
        $kode = $this->input->post('kd_poli');

        $data = array(
            'nm_poli' => $nama
        );

        $where = array(
            'kd_poli' => $kode
        );

        $this->model_petugas->update_data($where,$data,'poli');
        redirect('admin2/gotoPoli');
    }

    public function hapusDaftar($no_daftar)
    {
        $where = array('no_daftar' => $no_daftar);
		$this->model_petugas->deleteData($where,'pendaftaran_rekam');
        redirect('admin2/gotoDaftar');
    }

    public function hapus1($id_rekam)
    {
        $where = array('id_rekam' => $id_rekam);
		$this->model_petugas->deleteData($where,'dt_rekam');
        redirect('admin2/gotoRekam');
    }

    public function insertData()
    {
        $data = array(
            'kd_petugas' => $this->model_petugas->get_logged_user_id(),
            'no_daftar' => $this->input->post('no_daftar'),
            'idpasien' => $this->input->post('idpasien'),
            'tgl_periksa' => $this->input->post('tgl_periksa'),
            'penyakit' => $this->input->post('penyakit'),
            
		);
		if (!empty($_FILES['foto']['name'])) {
			$upload = $this->_do_upload();
			$data['foto'] = $upload;
		}
		$this->model_petugas->insertUpload($data);
		redirect('admin2/gotoRekam', $data);
    }

    public function chainedDropdwon()
    {
        $no_daftar = $_POST['no_daftar'];
	    $dropdown_chained = $this->model_petugas->getChained($no_daftar);
	    foreach ($dropdown_chained->result() as $baris) {
		echo "<option value='".$baris->idpasien."'>".$baris->nm_pasien."</option>";
	}
    }

    private function _do_upload()
	{
		$config['upload_path'] 		= './images/';
		$config['allowed_types'] 	= 'gif|jpg|png';
		$config['max_size'] 		= 2048;
		$config['max_widht'] 		= 1000;
		$config['max_height']  		= 1000;
		$config['file_name'] 		= round(microtime(true)*1000);
 
		$this->load->library('upload', $config);
		if (!$this->upload->do_upload('foto')) {
			$this->session->set_flashdata('msg', $this->upload->display_errors('',''));
			redirect('admin2/gotoRekam');
		}
		return $this->upload->data('file_name');
	}

}

/* End of file Admin2.php */


?>