<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        if($this->session->userdata('status') != "login"){
            redirect('login/login2');
        }
        $this->load->model('model_kepala');
        $this->load->model('model_petugas');
    }
    
    public function index()
    {
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('pages/kepala/content');
        $this->load->view('pages/kepala/footer');
    }

    function gotoPetugas() 
    {
        $data['userpetugas'] = $this->model_kepala->getData()->result();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/petugas',$data);
        $this->load->view('pages/kepala/footer');
    }

    function gotoUser1() 
    {
        $data['userpasien'] = $this->model_kepala->getDataUserPasien()->result();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/datapasien',$data);
        $this->load->view('pages/kepala/footer');
    }

    function gotoUser2() 
    {
        $data['userpetugas'] = $this->model_kepala->getData()->result();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/datapetugas',$data);
        $this->load->view('pages/kepala/footer');
    }

    function TambahPetugas() 
    {
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/petugas-tambah');
        $this->load->view('pages/kepala/footer');
    }

    function EditPetugas($kd_petugas) 
    {
        $where = array('kd_petugas' => $kd_petugas);
		$data['userpetugas'] = $this->model_kepala->edit_data($where,'petugas')->result();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/petugas-edit',$data);
        $this->load->view('pages/kepala/footer');
    }

    function HapusPetugas($kd_petugas) 
    {
        $where = array('kd_petugas' => $kd_petugas);
		$this->model_kepala->deletePetugas($where,'petugas');
        redirect('admin/gotoPetugas');
    }

    function gotoPasien() 
    {
        $data['dt_pasien'] = $this->model_kepala->getData1();
        $data['count'] = $this->model_kepala->getCount();
        $data['count1'] = $this->model_kepala->getCount1();
        $data['count2'] = $this->model_kepala->getCount2();
        $data1['penyakit'] = $this->model_petugas->getPenyakit();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/pasien',$data);
        $this->load->view('pages/kepala/footer');
    }

    function gotoPasienDetail() 
    {
        $data['dt_pasien'] = $this->model_kepala->getData1();
        $this->load->view('pages/kepala/header');
        $this->load->view('pages/kepala/sidebar');
        $this->load->view('kepala/detail',$data);
        $this->load->view('pages/kepala/footer');
    }

    public function actInputPetugas()
    {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $name = $this->input->post('nm_petugas');
        
        $dt_petugas = array(
            'username' => $user,
            'password' => $pass,
            'nm_petugas' => $name
        );

        $this->model_kepala->inputPetugas($dt_petugas,'petugas');
        redirect('admin/gotoPetugas');
    }

    public function actEditData()
    {
        $kd_petugas = $this->input->post('kd_petugas');
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $name = $this->input->post('nm_petugas');
        
        $data = array(
            'username' => $user,
            'password' => $pass,
            'nm_petugas' => $name
        );

        $where = array(
            'kd_petugas' => $kd_petugas
        );

        $this->model_kepala->update_data($where,$data,'petugas');
        redirect('admin/gotoPetugas');
    }


}

/* End of file Admin.php */

?>