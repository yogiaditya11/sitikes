<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_petugas extends CI_Model {

    public function getData()
    {
        return $this->db->get('pasien');
    }

    public function getData1()
    {
        return $this->db->get('poli');
    }

    public function getData2()
    {
        return $this->db->get('pendaftaran_rekam');
    }

    public function getDaftar()
    {
        $this->db->select('*');
        $this->db->from('pendaftaran_rekam');
        $this->db->join('pasien','pasien.idpasien=pendaftaran_rekam.idpasien');
        $this->db->join('poli','poli.kd_poli=pendaftaran_rekam.kd_poli');
        $query = $this->db->get();
        return $query->result();
    }

    public function getNo()
    {
        $query = $this->db->query("SELECT * FROM `pendaftaran_rekam` JOIN pasien USING (idpasien)");
        return $query->result();
    }

    public function getChained($no_daftar)
    {
        $query = $this->db->query("SELECT * FROM `pendaftaran_rekam` JOIN pasien USING (idpasien) WHERE no_daftar = '$no_daftar'");
	return $query;
    }
    public function getPenyakit()
    {
        $query = $this->db->query("SELECT * from penyakit");
        return $query->result();
    }

    public function getRekam()
    {
        $this->db->select('*');
        $this->db->from('v_insaa');
        // $this->db->select('*');
        // $this->db->from('dt_rekam');
        // $this->db->join('petugas','petugas.kd_petugas=dt_rekam.kd_petugas');
        // $this->db->join('pasien','pasien.idpasien=dt_rekam.idpasien');
        //$this->db->join('pendaftaran_rekam', 'pendaftaran_rekam.no_daftar = dt_rekam.no_daftar');
        $query = $this->db->get();
        return $query->result();
       
        // $query = $this->db->query("SELECT idpasien, nm_petugas, nm_pasien, COUNT(*) as jml FROM `v_as1` GROUP BY nm_pasien");
        // return $query->result();
    }

    public function getDetail()
    {
        $this->db->select('*');
        $this->db->from('v_as1');
        $this->db->where('idpasien', $this->get_logged_user_id1());
        $query = $this->db->get();
        return $query->result();
        // $query = $this->db->query('SELECT idpasien, nm_petugas, nm_pasien, tgl_periksa, foto FROM `v_as1` WHERE idpasien = "$this->get_logged_user_id1()" ');
        // return $query->result();
    }

    public function inputData($data,$table)
    {
        $this->db->insert($table,$data);
    }

    public function deleteData($where,$table)
    {
        $this->db->where($where);
	    $this->db->delete($table);
    }

    public function edit_data($where,$table)
    {
        return $this->db->get_where($table,$where);
    }

    public function update_data($where,$data,$table)
    {
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    public function upload(){
		$config['upload_path'] = './images/';
		$config['allowed_types'] = 'jpg|png|jpeg';
		$config['max_size']	= '2048';
		$config['remove_space'] = TRUE;
	
		$this->load->library('upload', $config); // Load konfigurasi uploadnya
		if($this->upload->do_upload('input_gambar')){ // Lakukan upload dan Cek jika proses upload berhasil
			// Jika berhasil :
			$return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
			return $return;
		}else{
			// Jika gagal :
			$return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
			return $return;
		}
    }

    public function insertUpload($data)
    {
        $this->db->insert('dt_rekam', $data);
    }
    
    public function get_logged_user_id()
	{
		$hasil = $this->db
						->select('kd_petugas')
						->where('username', $this->session->userdata('username'))
						->limit(1)
						->get('petugas');
		if($hasil->num_rows() > 0){
			return $hasil->row()->kd_petugas;
		}else{
			return 0;
		}
    }

    public function get_logged_user_id1()
	{
		$hasil = $this->db
						->select('idpasien')
						->get('v_as1');
		if($hasil->num_rows() > 0){
			return $hasil->row()->idpasien;
		}else{
			return 0;
		}
    }
}

/* End of file Model_petugas.php */

?>