<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pasien extends CI_Model {

    public function getData()
    {
        // $this->db->select('b.nm_petugas, a.tgl_periksa, a.foto, c.username');
        // $this->db->from('dt_rekam a');
        // $this->db->join('petugas b','b.kd_petugas=a.kd_petugas');
        // $this->db->join('pasien c','c.idpasien=a.idpasien');
        // $this->db->where('c.username', $this->session->userdata('username'));
        // //$this->db->join('pendaftaran_rekam', 'pendaftaran_rekam.no_daftar = dt_rekam.no_daftar');
        // $query = $this->db->get();
        // return $query->result();

        $this->db->select('*');
        $this->db->from('v_insaa');
        $this->db->where('uname_pasien', $this->session->userdata('username'));
        $query = $this->db->get();
        return $query->result();
    }

    public function getuser()
    {
        $this->db->select('DISTINCT(nm_pasien) AS nama');
        $this->db->from('pasien');
        $this->db->where('username', $this->session->userdata('username'));
        $query = $this->db->get();
        return $query->result();
    }
    

}

/* End of file Model_pasien.php */

?>