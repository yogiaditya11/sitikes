<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_kepala extends CI_Model {

    public function getData()
    {
        return $this->db->get('petugas');
    }

    public function getCount()
    {
        $this->db->select('COUNT(*) as jumlah');
        $this->db->from('pasien');
        $query = $this->db->get();
        return $query->result();
    }

    public function getCount1()
    {
        $query = $this->db->query("SELECT COUNT(*) AS jml FROM pasien WHERE kelamin='Pria'");
        return $query->result();
    }

    public function getCount2()
    {
        $query = $this->db->query("SELECT COUNT(*) AS jml FROM pasien WHERE kelamin='Wanita'");
        return $query->result();
    }

    public function getData1()
    {
        // $this->db->select('*');
        // $this->db->from('dt_rekam');
        // $this->db->join('petugas','petugas.kd_petugas=dt_rekam.kd_petugas');
        // $this->db->join('pasien','pasien.idpasien=dt_rekam.idpasien');
        $this->db->select('*');
        $this->db->from('v_insaa');
        $query = $this->db->get();
        return $query->result();
    }

    public function getDataUserPasien()
    {
        return $this->db->get('pasien');
    }

    public function inputPetugas($data,$table)
    {
        $this->db->insert($table,$data);
    }

    public function deletePetugas($where,$table)
    {
        $this->db->where($where);
	    $this->db->delete($table);
    }

    public function edit_data($where,$table)
    {
        return $this->db->get_where($table,$where);
    }

    public function update_data($where,$data,$table)
    {
        $this->db->where($where);
        $this->db->update($table,$data);
    }

}

/* End of file Model_kepala.php */

?>