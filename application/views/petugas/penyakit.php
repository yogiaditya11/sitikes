<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Data Pasien
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pasien</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th>Nama Pasien</th>
                  <th>Alamat</th>
                  <th>Golongan Darah</th>
                  <th>No HP</th>
                  <th>Jenis Kelamin</th>
                  <th>Tanggal Lahir</th>
                  <th><a href="<?php echo base_url();?>admin2/gotoPasienAdd" class="btn btn-flat btn-primary btn-sm btn-block"><span class="glyphicon glyphicon-plus"></span></a></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($pasien as $p) {
                ?>
                <tr>
                  <td><?= $no++?></td>
                  <td><?= $p->username ?></td>
                  <td><?= $p->password ?></td>
                  <td><?= $p->nm_pasien ?></td>
                  <td><?= $p->alamat ?></td>
                  <td><?= $p->gol_darah ?></td>
                  <td><?= $p->nohp ?></td>
                  <td><?= $p->kelamin ?></td>
                  <td><?= $p->tgl_lahir ?></td>
                  <td>
                  <a href="<?php echo base_url('admin2/gotoPasienEdit/'.$p->idpasien);?>" class="btn btn-flat btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                  <a href="<?php echo base_url('admin2/gotoHapusPasien/'.$p->idpasien);?>" class="btn btn-flat btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
    </section>
    <!-- /.content -->
  </div>