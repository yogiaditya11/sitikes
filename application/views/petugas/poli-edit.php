<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Edit Poli
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- form start -->
        <?php foreach($polii as $p){ ?>
        <form class="form-horizontal" method="POST" action=<?php echo base_url('admin2/actEditPoli');?>>
              <div class="box-body">
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nama Poli</label>

                  <div class="col-sm-4">
                  <input type="hidden" class="form-control" id="inputEmail3" placeholder="Username" name="kd_poli" value="<?= $p->kd_poli ?>">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Nama" name="nm_poli" value="<?= $p->nm_poli ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"> </label>

                  <div class="col-sm-4">
                    <button type="submit" class="btn btn-primary btn-flat"> <span class="glyphicon glyphicon-plus"> Edit</button>
                  </div>
                </div>
              </div>
              <!-- /.box-footer -->
            </form>
        <?php } ?>
    </section>
    <!-- /.content -->
  </div>