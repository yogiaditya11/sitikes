<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Halaman Rekam Medis
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Rekam</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-lg-5">
			<!-- form start -->
			<?=$this->session->flashdata('msg') ?>
			<form class="form-horizontal" action="<?php echo base_url('admin2/insertData') ?>" method="POST"
				enctype="multipart/form-data">
				<div class="box-body">

				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">No Daftar</label>

					<div class="col-sm-6">
						<select name="no_daftar" id="no_daftar" class="form-control" required>
							
							<?php foreach($no1 as $pp) { ?>
								<option value="<?= $pp->no_daftar; ?>"><?= $pp->no_daftar; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Pasien</label>

						<div class="col-sm-6">
							<select name="idpasien" id="idpasien" class="form-control" required>
								<option value="#">---Pilih Pasien---</option>
								<!-- <?php foreach($pasien as $pp) { ?>
								<option value="<?= $pp->idpasien; ?>"><?= $pp->nm_pasien; ?></option>
								<?php } ?> -->
							</select>
						</div>
					</div>

					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Periksa</label>

						<div class="col-sm-6">
							<input type="date" class="form-control" id="inputPassword3" name="tgl_periksa" required>
						</div>
					</div>

					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Penyakit</label>

						<div class="col-sm-6">
						<input type="text" class="form-control" id="inputPassword3" name="penyakit" required>
						</div>
					</div>

					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label">Upload Rekam</label>

						<div class="col-sm-6">
							<input type="file" class="form-control" id="inputPassword3" name="foto" required>
						</div>
					</div>

					<div class="form-group">
						<label for="inputPassword3" class="col-sm-3 control-label"> </label>

						<div class="col-sm-6">
						<button type="submit" class="btn btn-primary btn-flat"> <span class="glyphicon glyphicon-plus"> Tambah</button>
						</div>
					</div>
				</div>
				<!-- /.box-footer -->
			</form>
		</div>

		<div class="col-lg-7">

			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Panel title</h3>
				</div>
				<div class="panel-body">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Pasien</th>
								<th>Poli</th>
								<th>Tanggal Periksa</th>
								<th>Penyakit</th>
								<th>Rekam Medis</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
                $no = 1;
                foreach($rekam as $u) {
                ?>
							<tr>
								<td><?= $no++?></td>
								<td><?= $u->nm_pasien ?></td>
								<td><?= $u->nm_poli ?></td>
								<td><?= $u->tgl_periksa ?></td>
								<td><?= $u->penyakit ?></td>
								<td><a href="<?php echo base_url('images/'.$u->foto);?>" class="MagicZoom" rel="zoom-id:zoom;opacity-reverse:true;">
								<img src="<?php echo base_url('images/'.$u->foto)?>" style="width:100px; height:100px;">
								</a>
								</td>
								<td><a href="<?php echo base_url('admin2/hapus1/'.$u->id_rekam);?>" class="btn btn-flat btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>

		</div>
	</section>
	<!-- /.content -->
</div>

