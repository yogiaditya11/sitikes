<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Halaman Pendaftaran
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Daftar</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<div class="col-lg-5">
		<!-- form start -->
		<form class="form-horizontal" method="POST" action="<?php echo base_url();?>admin2/actInputDaftar">
			<div class="box-body">
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Tanggal Daftar</label>

					<div class="col-sm-6">
						<input type="date" class="form-control" id="inputPassword3" name="tgl_daftar" required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Pasien</label>

					<div class="col-sm-6">
						<select name="idpasien" id="idpasien" class="form-control" required>
							<option value="#">---Pilih Pasien---</option>
							<?php foreach($pasien as $pp) { ?>
								<option value="<?= $pp->idpasien; ?>"><?= $pp->nm_pasien; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Poli</label>

					<div class="col-sm-6">
						<select name="kd_poli" id="kd_poli" class="form-control" required>
							<option value="#">---Pilih Poli---</option>
							<?php foreach($poli as $p) { ?>
								<option value="<?= $p->kd_poli; ?>"><?= $p->nm_poli; ?></option>
							<?php } ?>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label">Keluhan</label>

					<div class="col-sm-6">
						<textarea name="keluhan" id="keluhan" cols="26" rows="10" required></textarea>
					</div>
				</div>

				<div class="form-group">
					<label for="inputPassword3" class="col-sm-3 control-label"> </label>

					<div class="col-sm-6">
					<button type="submit" class="btn btn-primary btn-flat"> <span class="glyphicon glyphicon-plus"> Tambah</button>
					</div>
				</div>
			</div>
			<!-- /.box-footer -->
		</form>
	</div>
		
	<div class="col-lg-7">
		
		<div class="panel panel-default">
			  <div class="panel-heading">
					<h3 class="panel-title">Data Pendaftaran Rekam Medis</h3>
			  </div>
			  <div class="panel-body">
			  <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Tanggal Daftar</th>
                  <th>Nama Pasien</th>
                  <th>Nama Poli</th>
                  <th>Keluhan</th>
				  <th>Aksi</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($dftr as $df) {
                ?>
                <tr>
                  <td><?= $no++?></td>
                  <td><?= $df->tgl_daftar ?></td>
                  <td><?= $df->nm_pasien ?></td>
                  <td><?= $df->nm_poli ?></td>
                  <td><?= $df->keluhan ?></td>
				  <td><a href="<?php echo base_url('admin2/hapusDaftar/'.$df->no_daftar);?>" class="btn btn-flat btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
			  </div>
		</div>
		
	</div>
	</section>
	<!-- /.content -->
</div>
