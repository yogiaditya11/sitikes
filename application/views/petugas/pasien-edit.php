<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Edit Pasien
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- form start -->
        <?php foreach($pasien as $p){ ?>
        <form class="form-horizontal" action="<?php echo base_url('admin2/actEditPasien');?>" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Username</label>

                  <div class="col-sm-3">
                  <input type="hidden" class="form-control" id="inputEmail3" placeholder="Username" name="idpasien" value="<?= $p->idpasien ?>">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Username" name="username" value="<?= $p->username ?>">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Password" name="password" value="<?= $p->password ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Nama Pasien</label>

                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Nama" name="nm_pasien" value="<?= $p->nm_pasien ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Alamat</label>

                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Alamat" name="alamat" value="<?= $p->alamat ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Golongan Darah</label>

                  <div class="col-sm-3">
                    <select name="gol_darah" id="gol_darah" class="form-control">
                        <option value="#">---Pilih Golongan Darah---</option>
                        <option value="A">A</option>
                        <option value="B">B</option>
                        <option value="O">O</option>
                        <option value="AB">AB</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">No HP</label>

                  <div class="col-sm-3">
                    <input type="number" class="form-control" id="inputPassword3" placeholder="No HP" name="nohp" value="<?= $p->nohp ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jenis Kelamin</label>

                  <div class="col-sm-3">
                    <select name="kelamin" id="kelamin" class="form-control">
                        <option value="#">---Pilih Jenis Kelamin---</option>
                        <option value="Pria">Pria</option>
                        <option value="Wanita">Wanita</option>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Tanggal Lahir</label>

                  <div class="col-sm-3">
                    <input type="date" class="form-control" id="inputPassword3" name="tgl_lahir" value="<?= $p->tgl_lahir ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label"> </label>

                  <div class="col-sm-3">
                  <button type="submit" class="btn btn-primary btn-flat"> <span class="glyphicon glyphicon-plus"> Edit</button>
                  </div>
                </div>
              </div>
              <!-- /.box-footer -->
            </form>
        <?php } ?>
    </section>
    <!-- /.content -->
  </div>