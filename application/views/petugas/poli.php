<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Data Poli
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Poli</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Poli</th>
                  <th><a href="<?php echo base_url();?>admin2/gotoPoliAdd" class="btn btn-flat btn-primary btn-sm"><span class="glyphicon glyphicon-plus"></span></a></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($poli as $pp) {
                ?>
                <tr>
                  <td><?= $no++;?></td>
                  <td><?= $pp->nm_poli ?></td>
                  <td>
                  <a href="<?php echo base_url('admin2/gotoPoliEdit/'.$pp->kd_poli);?>" class="btn btn-flat btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span></a>
                  <a href="<?php echo base_url('admin2/gotoHapusPoli/'.$pp->kd_poli);?>" class="btn btn-flat btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
    </section>
    <!-- /.content -->
  </div>