<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Data User Petugas
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pasien</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Petugas</th>
                  <th>Username</th>
                  <th>Password</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($userpetugas as $up) {
                ?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $up->nm_petugas;?></td>
                  <td><?php echo $up->username;?></td>
                  <td><?php echo $up->password;?></td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
    </section>
    <!-- /.content -->
  </div>