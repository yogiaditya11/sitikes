<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Halaman Kelola Petugas
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Petugas</li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama Petugas</th>
                  <th>Username</th>
                  <th>Password</th>
                  <th><a href="<?php echo base_url();?>admin/TambahPetugas" class="btn btn-primary btn-sm btn-block btn-flat"><span class="glyphicon glyphicon-plus"></span></a></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                foreach($userpetugas as $up) {
                ?>
                <tr>
                  <td><?php echo $no++;?></td>
                  <td><?php echo $up->nm_petugas;?></td>
                  <td><?php echo $up->username;?></td>
                  <td><?php echo $up->password;?></td>
                  <td>
                    <a href="<?php echo base_url('admin/EditPetugas/'.$up->kd_petugas);?>" class="btn btn-warning btn-sm btn-sm btn-flat"><span class="glyphicon glyphicon-pencil"></span></a>
                    <a href="<?php echo base_url('admin/HapusPetugas/'.$up->kd_petugas);?>" class="btn btn-danger btn-sm btn-sm btn-flat"><span class="glyphicon glyphicon-trash"></span></a>
                  </td>
                </tr>
                <?php } ?>
                </tbody>
              </table>
    </section>
    <!-- /.content -->
  </div>