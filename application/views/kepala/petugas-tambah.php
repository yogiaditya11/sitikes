<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Halaman Kelola Petugas
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Petugas</li>
			<li class="active">Add</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<form class="form-horizontal" action="<?php echo base_url('admin/actInputPetugas');?>" method="POST">
			<div class="box-body">
				<div class="form-group">
					<label for="username" class="col-sm-2 control-label">Username</label>

					<div class="col-sm-4">
						<input type="text" class="form-control" id="username" placeholder="Username" name="username" required>
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-sm-2 control-label">Password</label>

					<div class="col-sm-4">
						<input type="password" class="form-control" id="password" placeholder="Password"
							name="password" required>
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-sm-2 control-label">Nama Petugas</label>

					<div class="col-sm-4">
						<input type="text" class="form-control" id="password" placeholder="Nama" name="nm_petugas" required>
					</div>
				</div>
			</div>

			<div class="form-group">
				<label for="password" class="col-sm-2 control-label"> </label>

				<div class="col-sm-4">
				<button type="submit" class="btn btn-info btn-flat"> <span class="glyphicon glyphicon-plus"> Add</button>
					<!-- <a href="" class="btn btn-primary btn-flat btn-md"><span class="glyphicon glyphicon-plus"></span>
						Add</a> -->
				</div>
			</div>
			<!-- /.box-body -->
		</form>

	</section>
	<!-- /.content -->
</div>
