<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Halaman Kelola Petugas
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li>Petugas</li>
			<li class="active">Edit</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<?php foreach($userpetugas as $u){ ?>
		<form class="form-horizontal" action="<?php echo base_url('admin/actEditData');?>" method="POST">
			<div class="box-body">
				<div class="form-group">
					<label for="username" class="col-sm-2 control-label">Username</label>

					<div class="col-sm-4">
					<input type="hidden" class="form-control" id="kd_petugas" name="kd_petugas" value="<?php echo $u->kd_petugas;?>">
						<input type="text" class="form-control" id="username" name="username" value="<?php echo $u->username;?>">
					</div>
				</div>
				<div class="form-group">
					<label for="password" class="col-sm-2 control-label">Password</label>

					<div class="col-sm-4">
						<input type="text" class="form-control" id="password" placeholder="Password" name="password" value="<?php echo $u->password;?>">
					</div>
				</div>
        <div class="form-group">
					<label for="password" class="col-sm-2 control-label">Nama Petugas</label>

					<div class="col-sm-4">
						<input type="text" class="form-control" id="password" placeholder="Nama" name="nm_petugas" value="<?php echo $u->nm_petugas;?>">
					</div>
				</div>
			</div>

      <div class="form-group">
					<label for="password" class="col-sm-2 control-label"> </label>

					<div class="col-sm-4">
					<button type="submit" class="btn btn-info btn-flat"> <span class="glyphicon glyphicon-pencil"> Edit</button>
					</div>
				</div>
			<!-- /.box-body -->
		</form>
	<?php } ?>
	</section>
	<!-- /.content -->
</div>
