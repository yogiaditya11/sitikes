<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Halaman Laporan Pasien
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Pasien</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="col-lg-12 col-xs-6">
			<!-- small box -->
			<div class="small-box bg-yellow">
				<div class="inner">
        <?php foreach($count as $c) { ?>
					<h3><?= $c->jumlah ?></h3>

					<p>Pasien</p>
        <?php } ?>
				</div>
				<div class="icon">
					<i class="ion ion-person"></i>
				</div>
				<!-- <i href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
		</div>

		<div class="col-lg-6 col-xs-3">
			<!-- small box -->
			<div class="small-box bg-aqua">
				<div class="inner">
        <?php foreach($count1 as $c) { ?>
					<h3><?= $c->jml ?></h3>

					<p>Pasien Pria</p>
        <?php } ?>
				</div>
				<div class="icon">
					<i class="ion ion-person"></i>
				</div>
				<!-- <i href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
		</div>

		<div class="col-lg-6 col-xs-3">
			<!-- small box -->
			<div class="small-box bg-red">
				<div class="inner">
        <?php foreach($count2 as $c) { ?>
					<h3><?= $c->jml ?></h3>

					<p>Pasien Wanita</p>
        <?php } ?>
				</div>
				<div class="icon">
					<i class="ion ion-person"></i>
				</div>
				<!-- <i href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> -->
			</div>
		</div>
    
    <div class="col-lg-12 col-xs-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">List Pasien</h3>
			</div>
			<div class="panel-body">
				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Nama Pasien</th>
							<th>Nama Poli</th>
							<th>Penyakit</th>
							<th>Tanggal Periksa</th>
							<th>Rekam Medis</th>
							
						</tr>
					</thead>
					<tbody>
						<?php
                $no = 1;
                foreach($dt_pasien as $dp) {
                ?>
						<tr>
							<td><?php echo $no++;?></td>
							<td><?php echo $dp->nm_pasien;?></td>
							<td><?php echo $dp->nm_poli;?></td>
							<td><?php echo $dp->penyakit;?></td>
							<td><?php echo $dp->tgl_periksa;?></td>
							<td>
								<a href="<?php echo base_url('images/'.$dp->foto);?>" class="MagicZoom"
									rel="zoom-id:zoom;opacity-reverse:true;">
									<img src="<?php echo base_url('images/'.$dp->foto);?>" id="myImg" style="width:150px; height:150px;">
								</a>
							</td>
						</tr>
						<?php  } ?>
					</tbody>
				</table>

				<!-- The Modal -->
				<div id="myModal" class="modal">
					<span class="close">&times;</span>
					<img class="modal-content" id="img01">
					<div id="caption"></div>
				</div>
			</div>
		</div>
    </div>


	</section>
	<!-- /.content -->


</div>
